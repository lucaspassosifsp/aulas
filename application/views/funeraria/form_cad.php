<br>
<div class="container mt-5 "><br>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <form method="POST" class="text-center border border p-5">
                <p class="h4 mb-4">Cadastro de Urnas</p>
                <input type="text" value="<?= isset($dados['titulo']) ? $dados['titulo'] : '' ?>" id="titulo" name="titulo"  class="form-control mb-4" placeholder="Produto">
                <input type="text" value="<?= isset($dados['img']) ? $dados['img'] : '' ?>" id="img" name="img"  class="form-control mb-4" placeholder="Imagem">
                <input type="text" value="<?= isset($dados['descr']) ? $dados['descr'] : '' ?>" id="descr" name="descr" class="form-control mb-4" placeholder="descrição">
                <input type="number" value="<?= isset($dados['preco']) ? $dados['preco'] : '' ?>" id="preco" name="preco" class="form-control" placeholder="Preço" aria-describedby="defaultRegisterFormPasswordHelpBlock">
                <button class="btn-sm btn-dark my-4 btn-block" type="submit">Salvar</button>
            </form>
            <p class='m-2'><input type='submit' class='btn-sm btn-dark' value='Voltar' onclick='history.go(-1)' /></p>
        </div>
    </div>
</div>