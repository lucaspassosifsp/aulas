<br><br><div class="container mx-auto">
<!-- Card -->
<div class="card card-cascade wider reverse wow fadeInRightBig mt-5 mx-auto ml-2 mr-2">

  <!-- Card image -->
  <div class="view view-cascade overlay">
    <img class="card-img-top" src="<?= base_url('assets/img/img2.jpg')?>" alt="Card image cap">
    <a href="#!">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <!-- Card content -->
  <div class="card-body card-body-cascade text-center">

    <!-- Title -->
    <h4 class="card-title"><strong>Funerária Semente</strong></h4>
    <!-- Subtitle -->
    <h6 class="font-weight-bold  py-2">Nossa História</h6>
    <!-- Text -->
    <p class=" text-center"> O momento da despedida é sempre doloroso. A Funerária Semente sabe que não dá para evitar esse capítulo da vida. Por isso, estamos há 15 anos plantando homens na terra. O resultado se traduz em conforto, tranquilidade e muita segurança em momento tão delicado.

A Funerária Semente acompanha a família para ajudar a proporcionar sua última homenagem ao ente querido. Toda sua infraestrutura é voltada para amparar e garantir que esses momentos difíceis sejam superados da forma mais serena possível. </p>


  </div>

</div>

</div>
<!-- Card -->