<div class="container mx-auto mt-5">
    <div class="row">
        <div class='col-md-12 mx-auto'>
            <h2 class="h1-responsive font-weight-bold text-center my-5">Contato</h2>
        </div>
    </div>
    <div class="row wow fadeInUp">
        <div class='col-md-6 mx-auto'>
            <?= $contato ?>
        </div>
        <div class='col-md-6 mx-auto'>
            <?= $mapa ?>
        </div>
    </div>
    <div class="row mx-auto wow fadeInUp">
        <div class="col-md-12 mx-auto">
            <h3 class="my-4 pb-2 text-lg-center " >Mais Informações:</h3>
            <ul class="text-lg-center list-unstyled ml-4">

                <li>
                <p><i class="fas fa-phone pr-2 mr-"></i>(11) 4002-8922    <i class="ml-5 fas fa-envelope pr-2"></i>enterro@gmail.com</p>
                </li>
                <li>
                <p><i class="fas fa-map-marker-alt pr-2"></i>Av. Benjamin Harris Hunicutt, 1327 - Portal dos Gramados, Guarulhos - SP</p>
                </li>
                <li>
                
                </li>
                <li>
                </li>
            </ul>
        </div>
    </div>
  
</div>