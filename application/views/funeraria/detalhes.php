<br><div class="container mx-auto mt-5 border">
	<h5 class='p-2 m-2 bg-dark text-white text-center'><?= $titulo ?></h5>			  
			  <div class='container text-black'>
			  		<div class='row m-2 p-2'>
			        	<div class='col text-center my-auto'><img src='<?= base_url('assets/img/'.$img) ?>' width='250' /></div>
			        </div>
					  	<div class='col'>
							<div class='text-center font-weight-bold my-auto'>Valor: </div>
						  	<div class='text-center'>R$ <?= $preco ?></div>
					  	</div>
					</div>
					<div class="row mt-3">
						<div class='col'>
							<div class='text-center font-weight-bold my-auto'>Descrição: </div>
						  	<div class='text-center'><?= $descr ?></div>
					  	</div>
					</div>
			  </div>
			<br />
			<p class='m-2'><input type='submit' class='btn-sm btn-dark' value='Voltar' onclick='history.go(-1)' /></p>
</div>