<br><br><div class="card border  container mt-5 mx-auto mb-5 ">
		<div class="container" id="comments">
			<!--Comments Section-->
			<div class="comments-list text-left">
				<div class="section-heading">
					<h3>Comentários <span class="badge blue">3</span></h3>
				</div>
				<!--First row-->
				<div class="row">
					<!--Image column-->
					<div class="col-sm-2 col-12">
						<img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-1.jpg">
					</div>
					<!--/.Image column-->
					<!--Content column-->
					<div class="col-sm-10 col-12">
						<a><h3 class="user-name">Elias</h3></a>
						<div class="card-data">
							<ul>
								<li class="comment-date"><i class="fa fa-clock-o"></i> 23/10/2015</li>
							</ul>
						</div>
						<p class="comment-text">Produtos muito mais baratos que nas outras lojas!</p>
					</div>
					<!--/.Content column-->
				</div>
				<!--/.First row-->
				<!--Second row-->
				<div class="row">
					<!--Image column-->
					<div class="col-sm-2 col-12">
						<img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg">
					</div>
					<!--/.Image column-->
					<!--Content column-->
					<div class="col-sm-10 col-12">
						<a><h3 class="user-name">Marcia </h3></a>
						<div class="card-data">
							<ul>
								<li class="comment-date"><i class="fa fa-clock-o"></i> 31/12/2015</li>
							</ul>
						</div>
						<p class="comment-text">Sempre compro aqui!</p>
					</div>
					<!--/.Content column-->
				</div>
				<!--/.Second row-->
				<!--Third row-->
				<div class="row">
					<!--Image column-->
					<div class="col-sm-2 col-12">
						<img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg">
					</div>
					<!--/.Image column-->
					<!--Content column-->
					<div class="col-sm-10 col-12">
						<a><h3 class="user-name">Josepha </h3></a>
						<div class="card-data">
							<ul>
								<li class="comment-date"><i class="fa fa-clock-o"></i> 13/09/2016</li>
							</ul>
						</div>
						<p class="comment-text">Produtos de excelente qualidade !
						</p>
					</div>
					<!--/.Content column-->
				</div>
				<!--/.Third row-->
			</div>
			<!--/Comments Section-->
		</div>
	</div>
	
	<div class="card-sm container  border " id="reply">
		<!--Section: Leave a reply (Not Logged In User)-->
		<section>

			<!--Leave a reply form-->
			<div class="reply-form container mt-5 mx-auto">
				<h1 class="section-heading">Deixe seu comentário ! </h1>
		
				<div class="md-form">
					<i class="fa fa-user prefix"></i>
					<input type="text" id="form22" class="form-control">
					<label for="form22">Seu nome</label>
				</div>

				<div class="md-form">
					<i class="fa fa-envelope prefix"></i>
					<input type="text" id="form32" class="form-control">
					<label for="form32">Seu email</label>
				</div>

				
				<div class="md-form">
					<i class="fa fa-pencil prefix"></i>
					<textarea type="text" id="form18" class="md-textarea"></textarea>
					<label for="form18">Sua mensagem</label>
				</div>

				<div class="text-center mb-5">
					<button class="btn btn-primary">Enviar</button>
				</div>
			</div>
			<!--/.Leave a reply form-->

		</section>
		<!--/Section: Leave a reply (Not Logged In User)-->
	</div >