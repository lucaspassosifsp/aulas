<div class="container mt-5">
<div class="col-md-6 mx-auto">
<form method="POST" class="text-center border border p-5">

    <p class="h4 mb-4 text-center">Modificar Texto</p>

    <label for="textInput">Título</label>
    <input type="text" name="titulo" id="titulo" class="form-control mb-4" placeholder="Título">

    <label for="textarea">Texto</label>
    <textarea name="texto" id="texto" class="form-control mb-4" placeholder="Texto"></textarea>

    <button class="btn btn-info btn-block my-4" type="submit">Alterar</button>
</form>
</div>
</div>
