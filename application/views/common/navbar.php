<!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar navbar-dark elegant-color-dark">
      <div class="container">

        <!-- Brand -->
        <a class="navbar-brand waves-effect" href="<?php echo site_url('funeraria')?>">
          <strong class="white-text">Semente</strong>
        </a>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link waves-effect" href="<?php echo site_url('funeraria')?>">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect"  href="<?php echo site_url('urnas')?>">Urnas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect"  href="<?php echo site_url('contato')?>">Contato</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect"  href="<?php echo site_url('sobre')?>">Sobre</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect"  href="<?php echo site_url('comentario')?>">Comentários</a>
            </li>
          </ul>


          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a class="btn btn-bd-download d-none d-lg-inline-block mb-3 mb-md-0 ml-md-3 white" href="<?php echo site_url('cadastro_prod')?>" class="nav-link waves-effect">
                ADMIN
              </a>
            </li>
          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

  </header>
  <!--Main Navigation-->