<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/Urnas.php';

 class ModelProdutos extends CI_Model{

    public function criar(){
        if(sizeof($_POST) == 0) return;

        $titulo = $this->input->post('titulo');
        $img = $this->input->post('img');
        $descr = $this->input->post('descr');
        $preco = $this->input->post('preco');
        $urna = new Urna($titulo,$img, $descr, $preco);
        $urna->save();
        
    }

    
    public function lista_urnas(){
        $html = '';
        $urna = new Urna();
        $data = $urna->getAll();
        $html .= '<table class="table mx-auto justify-content-center mt-4" >';
        $html .= '<tr><th scope="col" class="justify-content-center" >Imagem</th><th scope="col" >Titulo</th><th scope="col">Preço</th><th scope="col">Detalhar</th></tr>';
        foreach($data as $row){
            $html .= '<tr>';
            $html .= '<th scope="col"><img src="'.base_url('assets/img/'.$row['img']).'" width="120px"></th>';
            $html .= '<th scope="col">'.$row['titulo'].'</th>';
            $html .= '<th>R$'.$row['preco'].',00</th>';
            $html .='<th scope="col">'.$this->icones_detalhe($row['id']).'</th></tr>';                           
            }
                $html .= '</table>';
                return $html;
        }

        
    public function lista_editar(){
        $html = '';
        $urna = new Urna();
        $data = $urna->getAll();
        $html .= '<table class="table mx-auto justify-content-center mt-4" >';
        $html .= '<tr><th scope="col" class="justify-content-center" >Imagem</th><th scope="col" >Titulo</th><th scope="col">Preço</th><th scope="col">Modificar</th></tr>';
        foreach($data as $row){
            $html .= '<tr>';
            $html .= '<th scope="col"><img src="'.base_url('assets/img/'.$row['img']).'" width="120px"></th>';
            $html .= '<th scope="col">'.$row['titulo'].'</th>';
            $html .= '<th>R$'.$row['preco'].',00</th>';               
            $html .='<th scope="col">'.$this->icones($row['id']).'</th></tr>';
        }
                $html .= '</table>';
                return $html;
        }

    private function icones($id){
        $html = '';
        $html .= '<a href="'.base_url('funeraria/update/'.$id).'"><i class="far fa-edit mr-3 text-primary"></i></a>';
        $html .= '<a href="'.base_url('funeraria/delete/'.$id).'"><i class="far fa-trash-alt text-danger"></i></a>';
        $html .= '<a href="'.base_url('funeraria/detalhar/'.$id).'"><i class="fa fa-eye ml-3" aria-hidden="true"></i></a>';
        return $html;

    }

    private function icones_detalhe($id){
        $html = '';
        $html .= '<a href="'.base_url('funeraria/detalhar/'.$id).'"><i class="fa fa-eye ml-3" aria-hidden="true"></i></a>';
        return $html;

    }
    

    public function carrega_urna($id){
        $urna = new Urna();
        return $urna->getById($id);
    }

    public function atualizar($id){
        if(sizeof($_POST) == 0) return;

        $data = $this->input->post();
        $urna = new Urna();
        if($urna->update($data, $id))
            redirect('funeraria/urnas');    
    }

    public function delete($id){
        $urna = new Urna();
        $urna->delete($id);
    } 
            

    public function urna_data($id){
        $sql = "SELECT * FROM urnas WHERE id = $id";
        $res = $this->db->query($sql);
        $data = $res->result_array();
        return $data[0];
    }
    }


    

?>