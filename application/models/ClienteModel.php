<?php 

class ClienteModel extends CI_Model {
    private $table_data = array(
        array('Maria', 'Silva', '@mary'),
        array('João', 'Souza', '@joca'),
        array('José', 'Santos', '@zeca'),
        array('Pedro', 'Serra', '@pepe')
    );
    
    public function jumboData(){
        $v['titulo'] = 'Promoção do Dia das Crianças - 2019';
        $v['subtitulo'] = 'Corra para obter os preços mais baixos do mercado!';
        $v['descricao'] = 'Somente em Outubro/19 você concorre a duas bicicletas ergométricas por dia nas compras acima de R$ 5.000,00! Aproveite a sua sorte!';
        $v['rotulo_botao'] = 'Tô envolvido até o pescoço!!!';
        return $v;
    }

    public function tabela() {
       $table = new Tabela($this->table_data);
       return $table->getHTML();
        // $html = '<table class="table">';
        // $html .= $this->tableHeader();
        // $html .= $this->tableBody($this->table_data);
        // $html .= '</table>';
        // return $html;
    }

    private function tableBody($data){
        $cont = 1;
        $html = '<tbody>';  
        foreach ($data as $row) {
            $html .= '<tr>';
            $html .= '<th scope="row">'.($cont).'</th>';
            $html .= '<td><a href="'.base_url('cliente/detalhe/'.$cont).'">'.$row[0].'</a></td>';
            $html .= '<td>'.$row[1].'</td>';
            $html .= '<td>'.$row[2].'</td>';
            $html .= '</tr>';
            $cont++;
        }
        $html .= '</tbody>';
        return $html;
    }

    private function tableHeader(){
        $html = '<thead class="indigo darken-4 white-text">
                    <tr>
                        <th scope="col">Nº</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Sobrenome</th>
                        <th scope="col">Usuário</th>
                    </tr>
                </thead>';
        return $html;
    }

    public function detalhe($id){
        return $this->table_data[$id];
    }

}
