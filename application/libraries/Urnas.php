<?php

class Urna{
    private $titulo;
    private $img;
    private $descr;
    private $preco;

    private $db;
    function __construct($titulo = null, $img = null, $descr = null, $preco = null){
        $this->titulo = $titulo;
        $this->img = $img;
        $this->descr = $descr;
        $this->preco = $preco;

        $ci = &get_instance();
        $this->db = $ci->db;
    }

   public function save(){
        $sql = "INSERT INTO urnas (titulo,img, descr, preco) 
       VALUES ('$this->titulo','$this->img', '$this->descr', '$this->preco')"; 
        $this->db->query($sql);
    }

    public function getALL(){
        $sql = "SELECT * FROM urnas";
        $res = $this->db->query($sql);
        return $res->result_array();
     }

     public function getById($id){
        $rs = $this->db->get_where('urnas', "id = $id");
       return $rs->row_array();
    }

     public function update($data, $id){
        $this->db->update('urnas', $data, "id = $id");
        return $this->db->affected_rows();
    }

    public function delete($id){
        $this->db->delete('urnas', "id = $id");
    }    
  
}

?>