<?php

// um controlador é um gerador de páginas.
class Funeraria extends CI_Controller {
	
	public function index(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');     
        $this->load->view('funeraria/home');

        $this->load->model('TextoModel');
        $texto = $this->TextoModel->texto_select();
        redirect('funeraria/inicio/'.$texto.'', 'refresh');
        $this->load->view('common/footer');


    }

    public function inicio($texto){

        $this->load->view('common/header');
        $this->load->view('common/navbar');     
        $this->load->view('funeraria/home');

        $this->load->model('TextoModel');
        $data = $this->TextoModel->texto_data($texto);
        $v['texto_inicio'] = $this->load->view('funeraria/texto_index',$data,true);
        $this->load->view('funeraria/layout_index', $v);



        $this->load->view('common/footer');
    }


    public function urnas(){
		$this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('ModelProdutos', 'model');
        $v['listar'] = $this->model->lista_urnas();
        $this->load->view('funeraria/urnas', $v); 

        $this->load->view('common/footer');
    }


    public function sobre(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('funeraria/sobre');

        $this->load->view('common/footer');
    }


    public function detalhar($id){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('ModelProdutos');
        $data = $this->ModelProdutos->urna_data($id);
        $v['urna'] = $this->load->view('funeraria/detalhes',$data,true);
        $this->load->view('funeraria/detalhes_exibir',$v);
        $this->load->view('common/footer');
    }


    public function contato(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->helper('form');
        $v['mapa'] = $this->load->view('funeraria/maps', '', true);
        $v['contato'] = $this->load->view('funeraria/contato', '', true);
        $this->load->view('funeraria/contato_rodape', $v);
        $this->load->view('common/footer');
    }

    public function delete($id){
        $this->load->model('ModelProdutos','d');
        $this->d->delete($id);
        redirect('cadastro_prod');
    }

    public function form_cad(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('ModelProdutos','modelinho');
        $this->modelinho->criar();

        $this->load->view('funeraria/form_cad');

        $this->load->view('common/footer');

    }

    public function update($id){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('ModelProdutos','modelinho');
        $this->modelinho->atualizar($id);
        $data['dados'] = $this->modelinho->carrega_urna($id);

        $this->load->view('funeraria/form_cad',$data);
        $this->load->view('common/footer');
    }

    public function comentario(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->view('funeraria/comentario');

        $this->load->view('common/footer');

    }

    public function insere_urna(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->model('ModelProdutos', 'model');
        $this->model->criar();
        $this->load->view('funeraria/form_cad');
        $this->load->view('common/footer');

    }

    public function admin(){
        $this->load->view('common/header');
        $this->load->view('common/admin_navbar');


        $this->load->view('common/footer');
    
    }


    public function admin_urnas(){
		$this->load->view('common/header');
        $this->load->view('common/admin_navbar');
        $this->load->model('ModelProdutos', 'model');
        $v['listar'] = $this->model->lista_editar();
        $this->load->view('admin/admin_produtos', $v); 

        $this->load->view('common/footer');
    }

    public function admin_texto(){
        $this->load->view('common/header');
        $this->load->view('common/admin_navbar');

        $this->load->model('TextoModel');
        $this->TextoModel->criar();
        $v['form'] = $this->load->view('admin/form_texto','', TRUE);
        $this->load->view('admin/alterar_index', $v);

        $this->load->view('common/footer');
    }


}