-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 07-Out-2019 às 05:20
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `lp2_passos`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `texto`
--

CREATE TABLE `texto` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `texto`
--

INSERT INTO `texto` (`id`, `titulo`, `texto`, `time_stamp`) VALUES
(7, 'Semente', 'Nossos colaboradores recebem treinamento em psicologia do luto e estão capacitados para organizar despedidas personalizadas, feitas com delicadeza e respeito pelos sentimentos e desejos de cada família.\r\n\r\nAcreditamos que cemitérios podem (e devem) ser lugares cheios de vida. Por isso realizamos com frequência eventos e atividades terapêuticas que incentivam a expressão do luto e celebram a memória daqueles que já se foram.\r\n\r\nSomos cemitério, crematório, funerária, plano funeral – oferecemos solução completa em caso de morte. Nossas instalações são modernas e procuram trazer conforto e praticidade para todos os que passam pelo Semente(temos estacionamento, lanchonete, espaço kids, dentre outros).', '2019-10-07 02:49:24');

-- --------------------------------------------------------

--
-- Estrutura da tabela `urnas`
--

CREATE TABLE `urnas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `img` varchar(50) NOT NULL,
  `descr` text NOT NULL,
  `preco` int(11) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `urnas`
--

INSERT INTO `urnas` (`id`, `titulo`, `img`, `descr`, `preco`, `time_stamp`) VALUES
(21, ' URNA MARGARIDA ', '01.jpg', ' Tamanho Padrão: 190cm x 56cm x 34cm ', 1000, '2019-10-07 03:12:24'),
(22, ' URNA BROMÉLIA ', '02.jpg', ' Tamanho Padrão: 190cm x 56cm x 34cm ', 1000, '2019-10-07 03:14:12'),
(23, ' URNA CRUZEIRO ', '03.jpg', ' Tamanho Padrão: 190cm x 56cm x 34cm ', 1000, '2019-10-07 03:14:35'),
(24, ' URNA BÍBLIA ', '04.jpg', ' Tamanho Padrão: 190cm x 56cm x 34cm ', 1000, '2019-10-07 03:15:01'),
(25, ' URNA LÍRIO ', '05.jpg', ' Tamanho Padrão: 190cm x 56cm x 34cm ', 1000, '2019-10-07 03:15:24'),
(26, ' URNA CRISÂNTEMO ', '06.jpg', ' Tamanho Padrão: 190cm x 56cm x 34cm ', 1000, '2019-10-07 03:15:47'),
(27, ' URNA AZALÉA ', '07.jpg', ' Tamanho Padrão: 190cm x 56cm x 34cm ', 15000, '2019-10-07 03:17:01'),
(28, ' URNA CEREJEIRA ', '08.jpg', ' Tamanho Padrão: 190cm x 56cm x 34cm ', 2000, '2019-10-07 03:17:27');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `texto`
--
ALTER TABLE `texto`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `urnas`
--
ALTER TABLE `urnas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `texto`
--
ALTER TABLE `texto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `urnas`
--
ALTER TABLE `urnas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
